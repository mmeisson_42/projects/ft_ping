#ifndef PONG_H
# define PONG_H

# include <netinet/ip_icmp.h>
# include <sys/time.h>
# include "ft_ping.h"

# define HAS_KERNEL_BUG

typedef struct	s_pong
{
	struct ip		ip;
	struct icmphdr	icmph;
	char		data[DATA_LEN];
}		s_pong;

int		pong(s_context *ctx, int sequence, int id, struct timeval *initial_tv);

#endif
