#include <sys/time.h>
#include <stdlib.h>
#include "ft_ping.h"

double	compute_latency(struct timeval *origin)
{
	long			usec_latency;
	struct timeval		now;

	gettimeofday(&now, NULL);
	usec_latency = (now.tv_sec * USEC_PER_SEC + now.tv_usec) -
		(origin->tv_sec * USEC_PER_SEC + origin->tv_usec);
	return usec_latency / (double)USEC_PER_MSEC;
}

