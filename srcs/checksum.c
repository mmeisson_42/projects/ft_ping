#include <stdlib.h>

unsigned short	checksum(unsigned short *packet, size_t len)
{
	unsigned long		sum = 0;

	while (len > 1)
	{
		sum += *packet;
		packet++;
		len -= 2;
	}
	if (len == 1)
		sum += *(unsigned char *)packet;
	sum = (sum >> 16) + (sum & 0xffff);
	sum = sum + (sum >> 16);
	return (unsigned short)(~sum);
}

