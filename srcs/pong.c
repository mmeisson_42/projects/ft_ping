#include <netinet/ip_icmp.h>
#include <sys/time.h>
#include <stdio.h>
#include <errno.h>
#include "pong.h"
#include "libft.h"
#include "ft_ping.h"
#include "compute_latency.h"

extern int errno;

static char		*err_message(int type)
{
	char	*error;

	switch (type)
	{
		case ICMP_UNREACH:
			error = "Network Unreachable";
			break;
		case ICMP_SOURCEQUENCH:
			error = "Packet Lost";
			break;
		case ICMP_TIMXCEED:
			error = "Time To Live Excedeed";
			break;
		default:
			error = "An error occured";
	}
	return error;
}

static void	handle_wrong_response(
	s_context *ctx,
	s_pong *pong,
	const char *host_addr,
	int bytes_received
)
{
	if (ctx->options & OPT_VERB)
	{
		printf(
			"%d bytes from %s: icmp_seq=%d type=%d code=%d\n",
			bytes_received,
			host_addr,
			ntohs(pong->icmph.un.echo.sequence),
			pong->icmph.type,
			pong->icmph.code
		);
	}
	else
	{
		const char *error = err_message(pong->icmph.type);

		printf(
			"%d bytes from %s: icmp_seq=%d %s\n",
			bytes_received,
			host_addr,
			ntohs(pong->icmph.un.echo.sequence),
			error
		);
	}
}

int			pong(s_context *ctx, int sequence, int id, struct timeval *initial_tv)
{
	s_pong		pong = {0};
	struct sockaddr_in	sin;
	struct iovec		iov = {
		.iov_base = &pong,
		.iov_len = sizeof pong,
	};
	struct msghdr		msg = {
		.msg_iov = &iov,
		.msg_iovlen = 1,
		.msg_name = &sin,
		.msg_namelen = sizeof sin,
	};

	int res_recv = recvmsg(ctx->socket_fd, &msg, 0);
	if (res_recv == -1)
	{
		return errno == 11 ? 1 : -1;
	}
	else
	{
		res_recv -= sizeof pong.ip;
		if (
				ntohs(pong.icmph.un.echo.sequence) == sequence
				&& pong.icmph.un.echo.id == id
		   )
		{
			if (pong.icmph.type == ICMP_ECHOREPLY && pong.icmph.code == 0)
			{
				const double latency = compute_latency(initial_tv);

				ctx->min = ctx->min == 0. ? latency : min(ctx->min, latency);
				ctx->max = max(ctx->max, latency);
				ctx->avg = ((ctx->avg * (double)ctx->received) + latency) / (ctx->received + 1);
				ctx->received += 1;
				printf(
					"%d bytes from %s: icmp_seq=%d ttl=%d time=%.2Fms\n",
					res_recv,
					ctx->addr,
					ntohs(pong.icmph.un.echo.sequence),
					pong.ip.ip_ttl,
					latency
				);
				return 1;
			}
			else if (pong.icmph.type != ICMP_ECHO)
			{
				handle_wrong_response(ctx, &pong, ctx->addr, res_recv);
			}
		}
		else if (pong.icmph.type != ICMP_ECHOREPLY)
		{
			handle_wrong_response(ctx, &pong, ctx->addr, res_recv);
			return 1;
		}
		return 0;
	}
	return -1;
}
