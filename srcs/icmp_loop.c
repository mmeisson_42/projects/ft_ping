#include <stdio.h>
#include <sys/socket.h>
#include <signal.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "ft_ping.h"
#include "ping.h"
#include "pong.h"
#include "libft.h"
#include "compute_latency.h"

struct s_context *g_ctx = NULL;

static void		activ_sleep(int seconds)
{
	volatile int	*activ = &g_ctx->activ_sleeping;

	*activ = 1;
	alarm(seconds);
	while (*activ == 1)
		;
	alarm(0);
}

static void		terminate_ping(
	__attribute__((unused)) int signal)
{
	unsigned long		miss_rate = 0;

	if (g_ctx->transmitted > 0)
	{
		miss_rate = (unsigned long)(
			((g_ctx->transmitted - g_ctx->received)
			/ (unsigned long)g_ctx->transmitted) * 100
		);
	}
	alarm(0);
	printf("\n--- %s ping statistics ---\n", g_ctx->host_name);
	printf(
		"%lu packets transmitted, %lu received, %lu%% packet loss, time %.0fms\n",
		g_ctx->transmitted,
		g_ctx->received,
		miss_rate,
		compute_latency(&g_ctx->start_time));
	if (g_ctx->received > 0)
	{
		printf(
			"rtt min/avg/max = %.3F/%.3F/%.3F\n",
			g_ctx->min,
			g_ctx->avg,
			g_ctx->max);
	}
	exit(g_ctx->lost != 0);
}

static void		activ_awake(
	__attribute__((unused)) int signal
)
{
	g_ctx->activ_sleeping = 0;
}

void			icmp_loop(struct s_context *ctx)
{
	u_short sequence = 0;
	struct s_ping packet = {
		.icmp = {
			.icmp_type = ICMP_ECHO,
			.icmp_code = 0,
			.icmp_id = (u_int16_t)getpid(),
		},
	};
	struct sockaddr_in dest = { .sin_family = AF_INET };
	struct timeval tv;

	g_ctx = ctx;
	signal(SIGINT, terminate_ping);
	signal(SIGALRM, activ_awake);
	ft_memset(&packet.icmp.icmp_data, 0, DATA_LEN);
	inet_pton(AF_INET, ctx->addr, &dest.sin_addr);

	printf(
		"PING %s (%s): %lu(%lu) bytes of data\n",
		ctx->host_name,
		ctx->addr,
		DATA_LEN,
		sizeof(s_ping)
	);

	gettimeofday(&ctx->start_time, NULL);
	while ((ctx->options & OPT_COUNT) == 0 || ctx->count-- > 0)
	{
		if (ping(ctx, &dest, packet, sequence, &tv) != -1)
		{
			int ponged;

			if ((ctx->options & OPT_TARD) == 0)
			{
				do
				{
					ponged = pong(ctx, sequence, packet.icmp.icmp_id, &tv);
				}
				while (ponged != 1);
				ctx->transmitted += 1;
				if (ponged > 0
					&& ((ctx->options & OPT_COUNT) == 0 || ctx->count > 0))
				{
					activ_sleep(1);
				}
				else
				{
					ctx->lost += 1;
				}
			}
			else
			{
				ft_putchar('.');
			}
		}
		else
		{
			printf("failed to ping\n");
		}
		sequence++;
	}
	terminate_ping(0);
}
