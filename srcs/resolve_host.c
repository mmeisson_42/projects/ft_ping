/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolve_host.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 18:26:13 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/18 18:26:14 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "libft.h"
#include "ft_ping.h"

char		*resolve_host(const char *addr)
{
	char				resolved[AI_ADDRLEN] = {0};
	static struct addrinfo		*addrinfo = NULL;

	if (addrinfo == NULL)
	{
		int got_addr = getaddrinfo(addr, NULL, NULL, &addrinfo);
		if (got_addr != 0 || addrinfo == NULL)
		{
			return NULL;
		}
	}
	inet_ntop(
		AF_INET,
		&((struct sockaddr_in *)addrinfo->ai_addr)->sin_addr,
		resolved,
		sizeof resolved
	);
	return ft_memdup(resolved, sizeof resolved);
}
