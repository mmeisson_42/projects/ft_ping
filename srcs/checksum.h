#ifndef CHECKSUM_H
# define CHECKSUM_H

# include <stdlib.h>

unsigned short		checksum(const void *data, size_t len);

#endif
