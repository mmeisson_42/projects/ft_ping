#include <stdio.h>
#include <stdlib.h>

void	helper(const char *command, int exit_status)
{
	printf("Usage: %s [-hv] [ -c count] [ -t ttl] [ -W timeout ] host\n", command);
	exit(exit_status);
}
