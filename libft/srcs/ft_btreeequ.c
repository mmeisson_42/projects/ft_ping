/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btreeequ.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 14:51:13 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/09 16:05:43 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		rot_right(t_btree **node)
{
	t_btree	*tmp;
	t_btree	*tmp2;

	tmp = (*node)->left->right;
	tmp2 = (*node);
	*node = (*node)->left;
	(*node)->right = tmp2;
	tmp2->left = tmp;
}

static void		rot_left(t_btree **node)
{
	t_btree	*tmp;
	t_btree	*tmp2;

	tmp = (*node)->right->left;
	tmp2 = (*node);
	*node = (*node)->right;
	(*node)->left = tmp2;
	tmp2->right = tmp;
}

static int		ft_btreechecker(t_btree **node)
{
	size_t		left;
	size_t		right;

	if (!node || !*node)
		return (0);
	left = ft_btreelevelcount((*node)->left);
	right = ft_btreelevelcount((*node)->right);
	if (left || right)
	{
		if ((int)(left - right) > 2)
		{
			rot_right(node);
			return (1);
		}
		if ((int)(right - left) > 2)
		{
			rot_left(node);
			return (-1);
		}
	}
	return (0);
}

void			ft_btreeequ(t_btree **btree)
{
	int		i;

	i = 2;
	if (*btree)
	{
		ft_btreeequ(&((*btree)->left));
		ft_btreeequ(&((*btree)->right));
		while (ft_btreechecker(btree) && i < 2)
			i++;
	}
}
