/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strover.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 20:56:24 by mmeisson          #+#    #+#             */
/*   Updated: 2015/11/29 15:46:21 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strover(char *erase, const char *str)
{
	char	*n_str;
	int		len_erase;
	int		len_str;

	len_erase = (erase) ? ft_strlen(erase) : 0;
	len_str = (str) ? ft_strlen(str) : 0;
	n_str = malloc(sizeof(char) * (len_erase + len_str + 1));
	if (n_str)
	{
		if (erase)
			ft_memcpy(n_str, erase, len_erase);
		if (str)
			ft_memcpy(n_str + len_erase, str, len_str + 1);
		if (erase)
			free(erase);
	}
	return (n_str);
}
