/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strreplace.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:32:10 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/10 13:32:38 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strreplace(char *str, char search, char replace)
{
	size_t	i;

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] == search)
		{
			str[i] = replace;
		}
		i++;
	}
	return (str);
}
