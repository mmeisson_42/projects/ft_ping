/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_whitesplit.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/28 19:44:22 by mmeisson          #+#    #+#             */
/*   Updated: 2017/09/26 10:39:35 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static size_t		count_words(const char *str)
{
	size_t		i;
	size_t		words;

	i = 0;
	words = 0;
	while (str[i])
	{
		while (ft_isspace(str[i]))
			i++;
		if (str[i])
			words++;
		while (str[i] && !ft_isspace(str[i]))
			i++;
	}
	return (words);
}

char				**ft_whitesplit(const char *str)
{
	char		**res;
	size_t		words_number;
	size_t		i;
	size_t		j;
	size_t		k;

	words_number = count_words(str);
	res = malloc(sizeof(char *) * (words_number + 1));
	if (res == NULL)
		return (res);
	i = 0;
	k = 0;
	while (k < words_number)
	{
		while (ft_isspace(str[i]))
			i++;
		j = 0;
		while (str[i + j] && !ft_isspace(str[i + j]))
			j++;
		res[k++] = ft_strndup(str + i, j);
		i += j + 1;
	}
	res[k] = NULL;
	return (res);
}
