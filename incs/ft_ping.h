#ifndef FT_PING_H
# define FT_PING_H

# define TYPE_ECHO			8
# define DEFAULT_TIMEOUT	1
# define DEFAULT_TTL		64

# define AI_ADDRLEN			32

# define ADDITIONAL_ICMP	36

# define DATA_LEN (ADDITIONAL_ICMP + sizeof ((struct icmp *)0)->icmp_dun)

# define USEC_PER_SEC		1000000
# define USEC_PER_MSEC		1000

# define BYTE_TO_WORD(len) ((len) >> 2)

# define OPT_VERB			0b1
# define OPT_COUNT			0b10
# define OPT_TARD			0b100

# define min(a, b) \
	({__typeof__(a) _a = (a) ; \
	__typeof__(b) _b = (b); \
	_a > _b ? _b : _a; })
# define max(a, b) \
	({__typeof__(a) _a = (a) ; \
	__typeof__(b) _b = (b); \
	_a < _b ? _b : _a; })

typedef struct	s_context
{
	char			*host_name;
	char			*addr;
	int				socket_fd;

	size_t			transmitted;
	size_t			received;
	size_t			lost;

	double			min;
	double			avg;
	double			max;
	double			mdev;

	struct timeval	start_time;
	int				activ_sleeping;

	int				options;
	int				count;
	int				ttl;
	int				timeout;
}		s_context;

#endif
