# *************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/08/28 14:28:00 by mmeisson          #+#    #+#              #
#    Updated: 2018/12/19 12:22:00 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			= ft_ping

CC				= gcc

CFLAGS			= -MD -Wall -Werror -Wextra -Ofast

VPATH			= ./srcs

SRCS			= ft_ping.c ping.c pong.c icmp_loop.c resolve_host.c \
		checksum.c compute_latency.c helper.c

INCS_PATHS		= ./incs/ ./srcs/ ./libft/incs/
INCS			= $(addprefix -I,$(INCS_PATHS))

OBJS_PATH		= ./.objs/
OBJS_NAME		= $(SRCS:.c=.o)
OBJS			= $(addprefix $(OBJS_PATH), $(OBJS_NAME))


DEPS			= $(OBJS:.o=.d)

LIB_PATHS		= ./libft/
LIBS			= $(addprefix -L,$(LIB_PATHS))

LDFLAGS			= $(LIBS) -lft



all: $(NAME)


$(NAME): $(OBJS)
	@$(foreach PATHS, $(LIB_PATHS),\
		echo "# # # # # #\n#";\
		echo '# \033[31m' Compiling $(PATHS) '\033[0m';\
		echo "#\n# # # # # #";\
		make -j32 -C $(PATHS);\
	)
	$(CC) $^ -o $@ $(LDFLAGS)

$(OBJS_PATH)%.o: $(SRCS_PATHS)%.c Makefile
	@mkdir -p $(OBJS_PATH)
	$(CC) $(CFLAGS) $(INCS) -o $@ -c $<

self_clean:
	rm -rf $(OBJS_PATH)

clean: self_clean
	@$(foreach PATHS, $(LIB_PATHS), make -C $(PATHS) clean;)

self_fclean:
	rm -rf $(OBJS_PATH)
	rm -f $(NAME)

fclean: self_fclean
	@$(foreach PATHS, $(LIB_PATHS), make -C $(PATHS) fclean;)

re:
	make fclean
	make -j32 all

-include $(DEPS)
